<?php

    class AvaliadorLeilao{
        
        private $maiorValor = 0;
        private $menorValor = INF;
        private $mediaLances = 0;
        
        
        
        public function avalia(Leilao $leilao){
            
            foreach( $leilao->getLances() as $lance){
                
                if( $lance->getValor() > $this->maiorValor ){
                    
                   $this->maiorValor = $lance->getValor();
                
                    
                }
                
                if( $lance->getValor() < $this->menorValor ){
                    
                        $this->menorValor = $lance->getValor();
                  
                }
                
                 $somaLances += $lance->getValor();
            }
            
                $this->mediaLances =$somaLances / count($leilao->getLances());
            
        }
        
        
        
        public function getMaiorValor(){
            
            return $this->maiorValor;
            
        }
        
        public function getMediaLances(){
            
            return $this->mediaLances;
            
        }
        
        public function getMenorValor(){
            
            return $this->menorValor;
            
        }
        
        
    }

?>