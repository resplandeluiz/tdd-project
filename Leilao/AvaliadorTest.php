<?php

    require 'Usuario.php';
    require 'Lance.php';
    require 'Leilao.php';
    require 'AvaliadorLeilao.php';

    class AvaliadorTest extends PHPUnit_Framework_TestCase{
        
        
        //O PREFIXO TESTE DEVE SER UTILIZADO APÓS COM A DESCRICAO DO MÉTODO
        public function testDeveAceitarLancesEmOrdemDescrecente(){
            
            
            $leilao = new Leilao("Xbox One");
    
            $luiz = new Usuario('Luiz',1);
            $manoel = new Usuario('Manoel',2);
            $resplande = new Usuario('Resplande',3);
        
            
            $leilao->propoe(new Lance($luiz,400));
            $leilao->propoe(new Lance($manoel,350));
            $leilao->propoe(new Lance($resplande,250));
            
            $leiloeiro = new AvaliadorLeilao();
            $leiloeiro->avalia($leilao);
            
            $maiorEsperao = 400;
            $menorEsperado = 250;
            
            
            //PARÂMETROS PRIMEIRO OQUE ESPERAMOS, DEPOIS O QUE ESTAMOS RECEBENDO E O TERCEIRO E O TAMANHO DO ERRO ACEITAVEL
            //0.00001-> QUE O NÚMERO PODE SER DIFERENTE DO OUTRO -> ARREDONDAMENTO
            
            $this->assertEquals( $maiorEsperao, $leiloeiro->getMaiorValor(), 0.00001);
            $this->assertEquals( $menorEsperado , $leiloeiro->getMenorValor() , 0.00001);
            
            
        }
        
        
        public function testCalcularAMediaDeLances(){
            
            $leilao = new Leilao("Xbox One");
    
            $luiz = new Usuario('Luiz',1);
            $manoel = new Usuario('Manoel',2);
            $resplande = new Usuario('Resplande',3);
        
            
            $leilao->propoe(new Lance($luiz,300));
            $leilao->propoe(new Lance($manoel,400));
            $leilao->propoe(new Lance($resplande,500));
            
            $mediaEsperada = 400;
            
            $leiloeiro = new AvaliadorLeilao();
            $valorRecebido = $leiloeiro->avalia($leilao);
            
            $this->assertEquals( $mediaEsperada, $leiloeiro->getMediaLances(), 0.00001);
           
            
            
            
        }
        
        
    }
    
    
    

?>