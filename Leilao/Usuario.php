<?php

    class Usuario{
        
        private $id;
        private $nome;
        
        
        function __construct($nome, $id = null){
            
            $this->id = $id;
            $this->nome = $nome;
            
        }
        
        
        public function getId(){
            
            return $this->id;
            
        }
        
        public function getNome(){
            
            return $this->nome;
        }
        
        
        
    }


?>